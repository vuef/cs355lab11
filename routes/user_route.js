/**
 * Created by Fern on 3/28/2016.
 */
var express = require('express');
var router = express.Router();
var userDal = require('../model/user_dal');
var stateDal = require('../model/state_dal');

router.get('/all', function(req, res) {
    userDal.GetAll(function (err, result) {
            if (err) throw err;
            res.render('user/displayAllUsers.ejs', {rs: result});
        }
    );
});


router.get('/', function (req, res) {
    userDal.GetByID(req.query.user_id, function (err, result) {
            if (err) throw err;

            res.render('user/displayUserInfo.ejs', {rs: result, user_id: req.query.user_id});
        }
    );
});

router.get('/create',function(reg,res){
    stateDal.GetAll(function(err, result) {
        if (err)
        {
            res.send("Error: " + err);
        }
        else{
            res.render('user/userFormCreate.ejs', {states: result});
        }
    }
    );
});

router.get('/save', function(req, res, next) {
    console.log("firstname equals: " + req.query.firstname);
    console.log("the lastname submitted was: " + req.query.lastname);
    console.log("the email submitted was: " + req.query.email);
    console.log("the password submitted was: "+ req.query.password);
    console.log("the state submitted was: " + req.query.state_id);
    userDal.Insert(req.query, function(err, result){
        if (err) {
            res.send(err);
        }
        else {
            res.send("Successfully saved the user.");
        }
    });
});

router.get('/delete', function(req, res){
    console.log(req.query);
    userDal.GetByID(req.query.user_id, function(err, result) {
        if(err){
            res.send("Error: " + err);
        }
        else if(result.length != 0) {
            userDal.DeleteById(req.query.user_id, function (err) {
                res.send(result[0].user_id + ' Successfully Deleted');
            });
        }
        else {
            res.send('User does not exist in the database.');
        }
    });
});


router.get('/edit', function(req, res){
    console.log('/edit user_id:' + req.query.user_id);
    userDal.GetByID(req.query.user_id, function(err, user_result){
        if(err) {
            console.log(err);
            res.send('error: ' + err);
        }
        else {
            console.log(user_result);
            stateDal.GetAll(function(err, state_result) {
                console.log(state_result);
                res.render('user/user_edit_form', {rs: user_result, states: state_result});
            });
        }
    });
});

router.post('/update_user', function(req,res){
    console.log(req.body);
    userDal.Update(req.body.user_id, req.body.first_name, req.body.last_name, req.body.email, req.body.password, req.body.state_id,
        function(err, result){
            var message;
            if(err) {
                console.log(err);
                message = 'error: ' + err;
            }
            else {
                message = 'success';
            }
            userDal.GetByID(req.body.user_id, function(err, user_info){
                console.log(user_info);
                res.redirect('/user/edit?user_id=' + req.body.user_id + '&message=' + message);
            });
        });
});
module.exports = router;