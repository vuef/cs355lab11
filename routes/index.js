var express = require('express');
var router = express.Router();
var userDal = require('../model/user_dal');
var rateDal = require('../model/rate_dal');

/* GET home page. */
router.get('/', function(req, res, next) {
  var data = {
    title : 'Express'
  }
  //res.render('index', { title: 'CS355', subtitle: 'Lab 8' });
  if(req.session.account === undefined) {
    res.render('index', data);
  }
  else {
    data.first_name = req.session.account.first_name;
    res.render('index', data);
  }
  /* GET Template Example
  router.get('/templatelink', function(req, res, next) {
    res.render('templateexample.ejs', { title: 'cs355'});
  });
  */
});

router.get('/authenticate', function(req, res) {
  userDal.GetByEmail(req.query.email, req.query.password, function (err, account) {
    if (err) {
      res.send(err);
    }
    else if (account == null) {
      res.send("User not found.");
    }
    else {
      req.session.account = account;
      console.log(account);
      if(req.session.originalUrl = '/login') {
        req.session.originalUrl = '/'; //don't send user back to login, instead forward them to the homepage.
      }
      res.redirect(req.session.originalUrl);

    }
  });
});

router.get('/login', function(req, res, next) {
  if(req.session.account) {
    res.render('/');
  }
  else {
    res.render('authentication/login.ejs');
  }

});

router.get('/logout', function(req, res) {
  req.session.destroy( function(err) {
    res.render('authentication/logout.ejs');
  });
});

router.get('/rate/all', function (req, res) {
  rateDal.GetAll(function (err, result) {
        if (err) throw err;
        res.render('rate/displayAvgRating.ejs', {rs: result});
      }
  );
});

router.get('/rate/create', function(req, res){
  if(req.session.account) {
    rateDal.GetAll(function(err, result){
      if(err) {
        res.send("Error: " + err);
      }
      else {
        res.render('rate/rateInsertForm.ejs', {movies: result, user:req.session.account});
      }
    });
  }
  else{
    res.render('authentication/login.ejs');
  }
});

router.get('/rate/insert', function (req, res) {
  rateDal.Insert(req.query.user_id, req.query.movie_id, req.query.rating,
      function (err, result) {
        var response = {};
        if(err) {
          response.message = err.message;
        }
        else {
          response.message = 'Success!';
        }
        res.json(response);
      });
});
module.exports = router;
