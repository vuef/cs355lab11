/**
 * Created by Fern on 4/11/2016.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetAll = function(callback) {
    connection.query('SELECT * FROM genre_name;',
        function (err, result) {
            callback(err, result);
        }
    );
}

exports.Insert = function(genre_name, callback) {
    var qry = "INSERT INTO genre_name (genre_name) VALUES (?)";
    connection.query(qry, genre_name, function(err, result){
        callback(err, result);
    });
}
