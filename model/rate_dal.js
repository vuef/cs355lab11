/**
 * Created by Fern on 4/11/2016.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetAll = function(callback) {
    connection.query('SELECT * FROM movie_avg_rating;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
}

exports.Insert = function(user_id, movie_id, rating, callback) {
        var values = [movie_id, user_id, rating];
        connection.query('INSERT INTO rating (movie_id, user_id, rating) VALUES (?, ?, ?)', values,
            function (err, result) {
                callback(err, result);
    });
}

