/**
 * Created by Fern on 4/11/2016.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetAll = function(callback) {
    connection.query('SELECT * FROM state;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
}

exports.Insert = function(state_name, callback) {
    var qry = "INSERT INTO state (state) VALUES (?)";
    connection.query(qry, state_name, function(err, result){
        callback(err, result);
    });
}